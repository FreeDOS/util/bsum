Begin3
Language:    FR, 850, French (Standard)
Title:       BSUM
Description: Calculer les sommes de contrôles BSD des fichiers
Summary:     bsum est un petit outil (256 octets !) qui calcule les sommes de contrôle  BSD des fichiers.
Keywords:    dos
End
