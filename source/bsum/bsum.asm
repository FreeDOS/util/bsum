;
; bsum - a DOS tool that computes the BSD checksum of a file
; requires a 8086-class PC, DOS 2+ and 17K of free RAM.
;
; To be assembled with NASM: nasm -f bin -o bsum.com bsum.asm
;
; bsum is distributed under the terms of the MIT License, as listed below.
;
; Copyright (C) 2017 Mateusz Viste
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to
; deal in the Software without restriction, including without limitation the
; rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
; sell copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in
; all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;

CPU 8086           ; restrict instruction set to 8086
org 100h           ; COM file originates at 100h

BUFLEN equ 16384   ; use a read buffer of 16K - using a bigger buffer didn't
                   ; improve performace significantly during my tests

; === CODE SEGMENT ===========================================================
section .text

; some initialization
;push ds            ; I use scasb which reads from ES:DI, so ES should be set
;pop es             ; to DS. I don't do it, because DOS takes care of that
cld                ; for all my string operations I will read forward
mov bp, 4C01h      ; bp stores the exit (AX) value passed to INT 21h

; is there a (non-empty) argument?
mov al, 20h        ; I'll scan [81h] for non-space chars
mov di, 81h        ; it would be more readable to do mov di, 81h and then
mov cl, [di-1]     ; mov cl, [80h], but the contraption here is a byte shorter
xor ch, ch
mov bx, cx         ; save cx (arg len) into bx, I might need it again soon
repe scasb
mov dx, MSGHLP     ; preset MSGHLP in case I need to quit with help screen
jz short PRINTANDQUIT ; no arg: print help screen and quit

; add a 0 terminator to the arg, so it can be passed to INT 21h fopen()
mov [bx+81h], byte 0 ; reuse arg's length (good thing I had it saved)
; set dx to the start of the argument (ignore leading white spaces)
lea dx, [bx+80h]
sub dx, cx

; open file
mov ax, 3D00h      ; DOS 2+ "open file", read-only - di contains the file name
int 21h
mov dx, MSGERR
jc short PRINTANDQUIT

; all good (file opened)
xchg ax, bx        ; save file handle to BX

xor di, di         ; DI will hold the result
mov dx, buff       ; DX needs to point to the destination buffer
; read some bytes (BX contains the file handle already)
NEXTCHUNK:
mov ah, 3Fh        ; DOS 2+ read from file
mov cx, BUFLEN     ; read BUFLEN bytes of data and write it into buff (DX)
int 21h
jnc short READOK

; I/O error -> close file and quit!
mov ah, 3Eh        ; DOS 2+ "close file" (bx contains the file handle already)
int 21h
mov dx, MSGERR
PRINTANDQUIT:      ; exit routine: expects DX to be set with a msg and quits
                   ; using value in BP
mov ah, 09h        ; print error message
int 21h
xchg ax, bp        ; smaller than doing a mov ax, 4C01h
int 21h

READOK:
xchg ax, cx        ; CX contains the amount of bytes read, so I can reuse AX
jcxz EOF           ; look out for EOF (0 bytes read)
xor ax, ax         ; reset AX (AL will be reused, but AH needs to be clean)
mov si, dx         ; SI points to read buffer
; ====== PERFORMANCE CRITICAL SECTION BELOW ======
DECODENEXT:
lodsb              ; load a byte from DS:SI into AL and INC SI (1 clk)
ror di, 1          ; (1 clk)
add di, ax         ; (1 clk)
dec cx             ; (1 clk)
jnz short DECODENEXT ; (1 clk) DEC CX +JNZ is bigger than LOOP but 3x faster
;loop DECODENEXT   ; (5/6 clk)
; ====== END OF PERFORMANCE CRITICAL SECTION ======

jmp short NEXTCHUNK

EOF:
; close file (BX contains the file handle already)
mov ah, 3Eh
int 21h
; move the result to bx and print it out
mov bx, di
mov cx, 0404h      ; group the two MOVs below into one operation
;mov ch, 4         ; how many digits I need to print out
;mov cl, 4         ; will be used to shift (SHR/SHL) nibbles

PRINTNEXTDIGIT:

mov al, bh         ; I'm only interested in the nibble at the left edge of BX
shr al, cl         ; CL is set to 4

; convert the AL nibble into an ASCII character of the range '0'..'F'
; 3 instructions below were suggested by Rugxulo (freedos-user, 2017-04-11)
cmp al, 0Ah
sbb al, 69h
das

; display the digit on screen using INT 21h, AH=2
xchg ax, dx        ; goal here is to do a mov dl, al (but xchg ax is shorter)
mov ah, 2          ; DOS 1+ "write DL to screen"
int 21h

shl bx, cl         ; move on to next nibble (CL is still set to 4, remember?)
dec ch
jnz short PRINTNEXTDIGIT

; print a CRLF after the checksum and quit with exit code 0
mov dx, CRLF
dec bp             ; bp contains 4C01h, but now I need to quit with success
jmp short PRINTANDQUIT


; === DATA SEGMENT ===========================================================
;section .data
; no need to specify a new section - tiny model implies DS == CS anyway (and I
; can save a byte or two on segment alignment)

MSGHLP db "bsum v1.1 - computes BSD checksums of files (MIT license)", 13, 10, "Copyright (C) 2017 Mateusz Viste", 13, 10, 10, "Usage: bsum file", 13, 10, "$"
MSGERR db "ERROR"
CRLF   db 13, 10, "$" ; this one *must* follow the ERROR string!

FILLER db " Mojmir V."  ; adjust the binary to 256 bytes

; === BSS SEGMENT ============================================================
section .bss
buff resb BUFLEN
